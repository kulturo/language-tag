/*
 * Created on Fri Feb 23 2018
 *
 * Copyright (c) 2018 INRIA
 */


///! This crate can handle language tags as defined in the [BCP47](https://tools.ietf.org/html/bcp47) standard.
///!
///! A language tag is the small string that describe a culture (language with script, region, etc).
///! Examples: "en-US" (english as used in US), "fr-FR" (french as used in France), "zh-cmn-Hans-CN" (Chinese, Mandarin, Simplified script, as used in China).
///!
///! To use this crate, add the relevant dependency in your Cargo.toml.
///!
///! ```toml
///! [dependencies]
///! language-tag = "0.9"
///! ```
///!
///! Then load the lib in your crate root:
///!
///! ```
///! extern crate language_tag;
///! ```
///!
///! # Examples
///!
///! This crate support the LanguageTag as defined in the standard but mostly you will probably be interested in the LangTag only (names are taken from the standard).
///! 
///! ## Query a parsed language tag
///!
///! ```
///! use language_tag::*;
///! use std::str::FromStr;
///!
///! let langtag = LangTag::from_str("zh-cmn-Hans-CN").unwrap();
///! 
///! assert!(langtag.get_language().get_mainlang() == "zh");
///! assert!(langtag.get_script().unwrap().str() == "Hans");
///! assert!(langtag.get_region().unwrap().str() == "CN");
///! ```
///!
///! ## Use the LangTagBuilder
///!
///! ```
///! use language_tag::*;
///!
///! let langtag = LangTagBuilder::new("zh")
///!                    .language_extension("cmn")
///!                    .script("Hans")
///!                    .region("CN")
///!                    .build().unwrap();
///!
///! assert!(langtag.get_language().get_mainlang() == "zh");
///! assert!(langtag.get_script().unwrap().str() == "Hans");
///! assert!(langtag.get_region().unwrap().str() == "CN");
///! ```

#[allow(dead_code)]
pub mod tags;

pub use tags::*;

// #[allow(unused_comparisons)]
pub(crate) mod parser;
