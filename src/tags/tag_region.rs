/*
 * Created on Thu Feb 22 2018
 *
 * Copyright (c) 2018 INRIA
 */

use std::fmt;

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct TagRegion {
    region: String
}

impl TagRegion {
    pub(crate) fn new(region: String) -> TagRegion {
        TagRegion { region: region.to_ascii_uppercase() }
    }

    pub fn get_region(&self) -> &str {
        &self.region
    }

    pub fn str(&self) -> &str {
        &self.region
    }
}

impl fmt::Display for TagRegion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.region)
    }
}

#[cfg(test)]
mod tests {
    use tags::TagRegion;
    use parser::region;

    #[test]
    fn test_region() {
        let test_vectors = vec![
            "CN",
            "FR",
            "ca",
            "hk",
        ];
        let truth = vec![
            TagRegion::new(format!("CN")),
            TagRegion::new(format!("FR")),
            TagRegion::new(format!("CA")),
            TagRegion::new(format!("HK"))
        ];

        for (i, test) in test_vectors.iter().enumerate() {
            let parsed = region(test);
            assert!(parsed.is_ok(), "Failed to parse {}: {}", test, parsed.err().unwrap());
            assert_eq!(parsed.unwrap(), truth[i]);
        }
    }

    #[test]
    fn test_fail_region() {
        let test_vectors = vec![
            "",
            "china",
            "x",
            "abc",
        ];

        for test in test_vectors.iter() {
            let parsed = region(test);
            assert!(parsed.is_err(), "Failed to parse {}", test);
        }
    }
}