/*
 * Created on Thu Feb 22 2018
 *
 * Copyright (c) 2018 INRIA
 */

use std::fmt;


/// language in BCP47
/// Describe a language with its optional extensions
#[derive(Debug,Clone,PartialEq,Eq)]
pub struct TagLanguage {
    mainlang: String,
    langexts: Vec<String>
}

impl TagLanguage {
    pub(crate) fn new(main: String, exts: Vec<String>) -> TagLanguage {
        TagLanguage { mainlang: main.to_ascii_lowercase(), langexts: exts.iter().map(|e|e.to_ascii_lowercase()).collect() }
    }

    pub fn get_mainlang(&self) -> &str {
        &self.mainlang
    }

    pub fn get_lang_extensions(&self) -> &Vec<String> {
        &self.langexts
    }

    pub fn get_lang_extensions_count(&self) -> usize {
        self.langexts.len()
    }

    pub(crate) fn set_mainlang(&mut self, mainlang: String) {
        self.mainlang = mainlang;
    }

    pub(crate) fn add_lang_extension(&mut self, langext: String) {
        self.langexts.push(langext);
    }
}

impl fmt::Display for TagLanguage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}-{}", self.mainlang, self.langexts.join("-"))
    }
}

#[cfg(test)]
mod tests {
    use parser::{language, extlang};
    use tags::TagLanguage;

    #[test]
    fn test_tag_language_extensions() {
        let test_vectors = vec![
            "cmn",
            "cde-fgh",
            "cde-fgh-ijk"
        ];
        let truth = vec![
            vec![format!("cmn")],
            vec![format!("cde"), format!("fgh")],
            vec![format!("cde"), format!("fgh"), format!("ijk")],
        ];

        for (i, test) in test_vectors.iter().enumerate() {
            let parsed = extlang(test);
            assert!(parsed.is_ok(), "Failed to parse {}: {}", test, parsed.err().unwrap());
            assert_eq!(parsed.unwrap(), truth[i]);
        }
    }

    #[test]
    fn test_tag_language() {
        let test_vectors = vec![
            "de",
            "fr",
            "ja",
            "zh-cmn",
            "ab-cde-fgh",
            "ab-cde-fgh-ijk"
        ];
        let truth = vec![
            TagLanguage::new(format!("de"), Vec::new()),
            TagLanguage::new(format!("fr"), Vec::new()),
            TagLanguage::new(format!("ja"), Vec::new()),
            TagLanguage::new(format!("zh"), vec![format!("cmn")]),
            TagLanguage::new(format!("ab"), vec![format!("cde"), format!("fgh")]),
            TagLanguage::new(format!("ab"), vec![format!("cde"), format!("fgh"), format!("ijk")]),
        ];

        for (i, test) in test_vectors.iter().enumerate() {
            let parsed = language(test);
            assert!(parsed.is_ok(), "Failed to parse {}: {}", test, parsed.err().unwrap());
            assert_eq!(parsed.unwrap(), truth[i]);
        }
    }

    #[test]
    fn test_fail_language() {
        let test_vectors = vec![
            "",
            "Hans",
            "fr-Latn",
            "ab-cde-fgh-ijk-lmn",
        ];

        for test in test_vectors.iter() {
            let parsed = language(test);
            assert!(parsed.is_err(), "Failed to parse {}", test);
        }
    }

}

