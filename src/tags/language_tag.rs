/*
 * Created on Thu Feb 22 2018
 *
 * Copyright (c) 2018 INRIA
 */

use tags::*;
use parser::*;

use std::str::FromStr;

#[derive(Debug,Clone,PartialEq,Eq)]
pub enum LanguageTag {
        LangTag(LangTag),
        PrivateUse(TagExtension),
        GrandFathered(String)
}

impl From<LangTag> for LanguageTag {
        fn from(langtag: LangTag) -> LanguageTag {
                LanguageTag::LangTag(langtag)
        }
}

impl FromStr for LanguageTag {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        languagetag(s).map_err(|e|format!("{}", e))
    }
}

impl LanguageTag {
        pub fn langtag(l: &str) -> Result<LanguageTag, String> {
                langtag(l)      .and_then(|tag|Ok(LanguageTag::LangTag(tag)))
                                .map_err(|e| format!("Failed to create langtag: {}", e))
        }

        pub fn privateuse(p: &str) -> Result<LanguageTag, String> {
                privateuse(p)   .and_then(|tag|Ok(LanguageTag::PrivateUse(tag)))
                                .map_err(|e| format!("Failed to create private use tag: {}", e))
        }

        pub fn grandfathered(g: &str) -> Result<LanguageTag, String> {
                grandfathered(g).and_then(|tag|Ok(LanguageTag::GrandFathered(tag)))
                                .map_err(|e| format!("Failed to create grandfathered tag: {}", e))
        }
}

#[cfg(test)]
mod tests {
    use parser::*;
    use tags::*;

    #[test]
    fn test_tag() {
        let test_vectors = vec![
                "x-whatever",
                "i-enochian",
                "zh-cmn-Hans-CN"
        ];
        let truth: Vec<LanguageTag> = vec![
                LanguageTag::privateuse("x-whatever").unwrap(),
                LanguageTag::grandfathered("i-enochian").unwrap(),
                LangTagBuilder::new("zh").language_extension("cmn").script("Hans").region("cn").build().unwrap().into()
        ];

        for (i, test) in test_vectors.iter().enumerate() {
            let parsed = languagetag(test);
            assert!(parsed.is_ok(), "Failed to parse {}: {}", test, parsed.err().unwrap());
            assert_eq!(parsed.unwrap(), truth[i]);
        }
    }

    #[test]
    fn test_fail() {
        let test_vectors = vec![
            "i-whatever"
        ];

        for test in test_vectors.iter() {
            let parsed = languagetag(test);
            assert!(parsed.is_err(), "Failed to parse {}", test);
        }
    }

}