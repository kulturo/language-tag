/*
 * Created on Thu Feb 22 2018
 *
 * Copyright (c) 2018 INRIA
 */

use std::fmt;

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct TagScript {
    script: String
}

impl TagScript {
    pub(crate) fn new(script: String) -> TagScript {
        let mut cap_script = script.to_ascii_lowercase();
        cap_script[0..1].make_ascii_uppercase();
        TagScript { script: cap_script }
    }

    pub fn get_script(&self) -> &str {
        &self.script
    }

    pub fn str(&self) -> &str {
        &self.script
    }
}

impl fmt::Display for TagScript {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.script)
    }
}

#[cfg(test)]
mod tests {
    use tags::TagScript;
    use parser::script;

    #[test]
    fn test_script() {
        let test_vectors = vec![
            "Hant",
            "Hans",
            "cyrl",
            "latn",
        ];
        let truth = vec![
            TagScript::new(format!("Hant")),
            TagScript::new(format!("Hans")),
            TagScript::new(format!("Cyrl")),
            TagScript::new(format!("Latn"))
        ];

        for (i, test) in test_vectors.iter().enumerate() {
            let parsed = script(test);
            assert!(parsed.is_ok(), "Failed to parse {}: {}", test, parsed.err().unwrap());
            assert_eq!(parsed.unwrap(), truth[i]);
        }
    }

    #[test]
    fn test_fail_script() {
        let test_vectors = vec![
            "",
            "Han",
            "cyrll",
            "latin",
        ];

        for test in test_vectors.iter() {
            let parsed = script(test);
            assert!(parsed.is_err(), "Failed to parse {}", test);
        }
    }
}