/*
 * Created on Thu Feb 22 2018
 *
 * Copyright (c) 2018 INRIA
 */

use std::fmt;
use std::str::FromStr;
use std::collections::HashSet;

use tags::*;
use parser::langtag;

/// This stores a LangTag that define a language (with script, country, ...) according to [BCP47](https://tools.ietf.org/html/bcp47) standard.
/// You have probably already seen some. Examples: "en-US" (english as used in US), "fr-FR" (french as used in France), "zh-cmn-Hans-CN" (Chinese, Mandarin, Simplified script, as used in China)
/// Two possibilities to create a LangTag: parse a string or use the builder.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct LangTag {
    language: TagLanguage,
    script: Option<TagScript>,
    region: Option<TagRegion>,
    variants: Vec<TagVariant>,
    extensions: Vec<TagExtension>,
    privateuse: Option<TagExtension>
}

impl LangTag {

    /// Create the LangTag from exhaustive parts
    pub(crate) fn from_parts(lang: TagLanguage, 
                        script: Option<TagScript>, 
                        region: Option<TagRegion>, 
                        variants: Vec<TagVariant>, 
                        extensions: Vec<TagExtension>, 
                        privateuse: Option<TagExtension>) -> Result<LangTag, String> {

        if LangTag::are_extensionr_unique(&extensions) == true {
            Ok(LangTag { language: lang, script, region, variants, extensions, privateuse })
        }
        else {
            Err(format!("Extensions must be uniquely defined by their singleton."))
        }
        
    }

    /// Check that extensions are unique: singletons must be unique
    fn are_extensionr_unique(extensions: &Vec<TagExtension>) -> bool {
        let mut singletons: HashSet<&str> = HashSet::new();

        for ext in extensions.iter() {
            let ext_sing = ext.get_singleton();

            if singletons.contains(ext_sing) {
                return false;
            }
            else {
                singletons.insert(ext_sing);
            }
        }

        true
    }

    /// Create a LangTag with minimal information: the language
    pub(crate) fn minimal(lang: TagLanguage) -> LangTag {
        LangTag { language: lang, script: None, region: None, variants: Vec::new(), extensions: Vec::new(), privateuse: None }
    }

    /// Get the TagLanguage containing the main language information and its extensions
    ///
    /// # Examples
    ///
    /// ```
    /// use language_tag::tags::*;
    /// use std::str::FromStr;
    ///
    /// let langtag = LangTag::from_str("zh-cmn").unwrap();
    /// let language = langtag.get_language();
    ///
    /// assert_eq!(language.get_mainlang(), "zh");
    /// assert_eq!(language.get_lang_extensions()[0], "cmn");
    /// ```
    pub fn get_language(&self) -> &TagLanguage {
        &self.language
    }

    pub(crate) fn get_mut_language(&mut self) -> &mut TagLanguage {
        &mut self.language
    }

    pub(crate) fn set_language(&mut self, lang: TagLanguage) {
        self.language = lang;
    }

    /// Get the TagScript containing the script information
    ///
    /// # Examples
    ///
    /// ```
    /// use language_tag::tags::*;
    /// use std::str::FromStr;
    ///
    /// let langtag = LangTag::from_str("en-Latn-US").unwrap();
    /// if let Some(ref script) = langtag.get_script() {
    ///     assert_eq!(script.str(), "Latn");
    /// }
    /// ```
    pub fn get_script(&self) -> Option<&TagScript> {
        if let Some(ref s) = self.script {
            Some(s)
        }
        else {
            None
        }
    }

    pub(crate) fn set_script(&mut self, script: Option<TagScript>) {
        self.script = script;
    }

    /// Get the TagRegion containing the region information
    ///
    /// # Examples
    ///
    /// ```
    /// use language_tag::tags::*;
    /// use std::str::FromStr;
    ///
    /// let langtag = LangTag::from_str("en-Latn-US").unwrap();
    /// if let Some(ref region) = langtag.get_region() {
    ///     assert_eq!(region.str(), "US");
    /// }
    /// ```
    pub fn get_region(&self) -> Option<&TagRegion> {
        if let Some(ref r) = self.region {
            Some(r)
        }
        else {
            None
        }
    }

    pub(crate) fn set_region(&mut self, region: Option<TagRegion>) {
        self.region = region;
    }

    pub fn get_variants(&self) -> &Vec<TagVariant> {
        &self.variants
    }

    pub(crate) fn set_variants(&mut self, variants: Vec<TagVariant>) {
        self.variants = variants;
    }

    pub(crate) fn add_variant(&mut self, variant: TagVariant) {
        self.variants.push(variant)
    }

    pub(crate) fn clear_variants(&mut self) {
        self.variants.clear()
    }

    pub fn get_extensions(&self) -> &Vec<TagExtension> {
        &self.extensions
    }

    pub(crate) fn set_extensions(&mut self, extensions: Vec<TagExtension>) {
        self.extensions = extensions;
    }

    pub(crate) fn add_extension(&mut self, extension: TagExtension) {
        self.extensions.push(extension);
    }

    pub(crate) fn clear_extensions(&mut self) {
        self.extensions.clear()
    }

    pub fn get_privateuse(&self) -> &Option<TagExtension> {
        &self.privateuse
    }

    pub(crate) fn set_privateuse(&mut self, privateuse: Option<TagExtension>) {
        self.privateuse = privateuse;
    }
}

impl FromStr for LangTag {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        langtag(s).map_err(|e|format!("{}", e))
    }
}

impl fmt::Display for LangTag {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut subtags: Vec<&str> = Vec::new();

        //language
        let language = self.get_language();
        subtags.push(language.get_mainlang());

        for ext in language.get_lang_extensions() {
            subtags.push(ext);
        }
        
        //script
        if let Some(ref script) = self.get_script() {
            subtags.push(script.get_script());
        }

        //region
        if let Some(ref region) = self.get_region() {
            subtags.push(region.get_region());
        }

        //variants
        for variant in self.variants.iter() {
            subtags.push(variant.get_variant());
        }

        //extension
        for extension in self.extensions.iter() {
            subtags.push(extension.get_singleton());
            for extcontent in extension.get_tags().iter() {
                subtags.push(extcontent);
            }
        }

        //private use
        if let &Some(ref extension) = self.get_privateuse() {
            subtags.push(extension.get_singleton());
            for extcontent in extension.get_tags().iter() {
                subtags.push(extcontent);
            }
        }

        write!(f, "{}", subtags.join("-"))
    }
}

#[test]
fn test_parse_and_print() {
    let test_vectors = vec![
        "de",
        "fr-FR",
        "zh-cmn",
        "zh-cmn-Hans-CN",
        "zh-Hans-CN",
        "sl-rozaj-biske",
        "hy-Latn-IT-arevela",
        "es-419",
        "de-CH-x-phonebk",
        "zh-CN-a-myext-x-private",
        "en-a-myext-b-another",
        "sr-Cyrl",
        "zh-yue-HK",
        "de-CH-1901"
    ];

    for test in test_vectors {
        let parsed = LangTag::from_str(test);
        assert!(parsed.is_ok(), "Failed to parse {}: {}", test, parsed.err().unwrap());
        assert_eq!(parsed.unwrap().to_string(), test, "{} failed to be parsed", test);
    }
}