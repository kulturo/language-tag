/*
 * Created on Fri Feb 23 2018
 *
 * Copyright (c) 2018 INRIA
 */

use tags::*;
use parser::*;

/// Builder for LangTag: ensure that the formatting is correct at build
pub struct LangTagBuilder {
    building: Result<LangTag, String>,//defer error message to the end (build method)
}


impl LangTagBuilder {

    /// Setup a new LangTagBuilder with minimally required information: a language tag.
    pub fn new(lang: &str) -> LangTagBuilder {
        // if let Ok(taglang) = language(lang) {
        //     let building = LangTag::minimal(taglang);
        //     LangTagBuilder { building: Ok(building) }
        // }
        if let Ok(langtag) = langtag(lang)
        {
            LangTagBuilder { building: Ok(langtag) }
        }
        else {
            LangTagBuilder { building: Err(format!("Invalid language tag: {}.", lang)) }
        }
    }

    pub fn build(self) -> Result<LangTag, String> {
        self.building
    }

    pub fn language_extension(self, lang_ext: &str) -> LangTagBuilder {
        if let Ok(mut building) = self.building {
            if let Ok(tag_langext) = extlang_unique(lang_ext) {
                let previous_count = building.get_language().get_lang_extensions_count();
                if previous_count >= 3 {
                    LangTagBuilder { building: Err(format!("Invalid language extension count: maximum is 3.")) }
                }
                else {
                    building.get_mut_language().add_lang_extension(tag_langext);
                    LangTagBuilder { building: Ok(building) }
                }
            }
            else {
                LangTagBuilder { building: Err(format!("Invalid language extension tag: {}.", lang_ext)) }
            }
        }
        else {
            self
        }
    }

    pub fn script(self, new_script: &str) -> LangTagBuilder {
        if let Ok(mut building) = self.building {
            if let Ok(tag_script) = parser::script(new_script) {
                building.set_script(Some(tag_script));
                LangTagBuilder { building: Ok(building) }
            }
            else {
                LangTagBuilder { building: Err(format!("Invalid script tag: {}.", new_script)) }
            }
        }
        else {
            self
        }
    }

    pub fn region(self, new_region: &str) -> LangTagBuilder {
        if let Ok(mut building) = self.building {
            if let Ok(tag_region) = parser::region(new_region) {
                building.set_region(Some(tag_region));
                LangTagBuilder { building: Ok(building) }
            }
            else {
                LangTagBuilder { building: Err(format!("Invalid region tag: {}.", new_region)) }
            }
        }
        else {
            self
        }
    }

    pub fn extension(self, new_extension: &str) -> LangTagBuilder {
        if let Ok(mut building) = self.building {
            if let Ok(tag_extension) = parser::extension(new_extension) {
                building.add_extension(tag_extension);
                LangTagBuilder { building: Ok(building) }
            }
            else {
                LangTagBuilder { building: Err(format!("Invalid extension tag: {}.", new_extension)) }
            }
        }
        else {
            self
        }
    }

    pub fn variant(self, new_variant: &str) -> LangTagBuilder {
        if let Ok(mut building) = self.building {
            if let Ok(tag_variant) = parser::variant(new_variant) {
                building.add_variant(tag_variant);
                LangTagBuilder { building: Ok(building) }
            }
            else {
                LangTagBuilder { building: Err(format!("Invalid variant tag: {}.", new_variant)) }
            }
        }
        else {
            self
        }
    }

    pub fn privateuse(self, new_privateuse: &str) -> LangTagBuilder {
        if let Ok(mut building) = self.building {
            if let Ok(tag_privateuse) = parser::privateuse(new_privateuse) {
                building.set_privateuse(Some(tag_privateuse));
                LangTagBuilder { building: Ok(building) }
            }
            else {
                LangTagBuilder { building: Err(format!("Invalid private use tag: {}.", new_privateuse)) }
            }
        }
        else {
            self
        }
    }
}

impl From<LangTag> for LangTagBuilder {
    /// Allow to extend a LangTag by converting it to a builder
    fn from(langtag: LangTag) -> LangTagBuilder {
        LangTagBuilder { building: Ok(langtag) }
    }
}

#[test]
fn test_builder() {
    let build1 = LangTagBuilder::new("fr").script("Latn").region("FR").build();
    assert!(build1.is_ok());
}