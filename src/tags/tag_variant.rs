/*
 * Created on Thu Feb 22 2018
 *
 * Copyright (c) 2018 INRIA
 */

use std::fmt;

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct TagVariant {
    variant: String
}

impl TagVariant {
    pub(crate) fn new(variant: String) -> TagVariant {
        TagVariant { variant: variant }
    }

    pub fn get_variant(&self) -> &str {
        &self.variant
    }
}

impl fmt::Display for TagVariant {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.variant)
    }
}