/*
 * Created on Thu Feb 22 2018
 *
 * Copyright (c) 2018 INRIA
 */


use std::fmt;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TagExtension {
    singleton: String,//string and not char to be able to get &str
    tags: Vec<String>
}

impl TagExtension {
    pub(crate) fn new(singleton: char, tags: Vec<String>) -> TagExtension {
        TagExtension { singleton: singleton.to_string(), tags }
    }

    pub fn get_tags(&self) -> &Vec<String> {
        &self.tags
    }

    pub fn get_singleton(&self) -> &str {
        &self.singleton
    }
}

impl fmt::Display for TagExtension {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}-{}", self.singleton, self.tags.join("-"))
    }
}

#[cfg(test)]
mod tests {
    use parser::*;
    use tags::*;

    #[test]
    fn test_extension() {
        let test_vectors = vec![
            "u-myext",
            "a-ext11-ext12"
        ];
        let truth = vec![
            TagExtension::new('u', vec![format!("myext")]),
            TagExtension::new('a', vec![format!("ext11"), format!("ext12")])
        ];

        for (i, test) in test_vectors.iter().enumerate() {
            let parsed = extension(test);
            assert!(parsed.is_ok(), "Failed to parse {}: {}", test, parsed.err().unwrap());
            assert_eq!(parsed.unwrap(), truth[i]);
        }
    }

    #[test]
    fn test_privateuse() {
        let test_vectors = vec![
            "x-phonebk",
            "x-ext11-ext12"
        ];
        let truth = vec![
            TagExtension::new('x', vec![format!("phonebk")]),
            TagExtension::new('x', vec![format!("ext11"), format!("ext12")])
        ];

        for (i, test) in test_vectors.iter().enumerate() {
            let parsed = privateuse(test);
            assert!(parsed.is_ok(), "Failed to parse {}: {}", test, parsed.err().unwrap());
            assert_eq!(parsed.unwrap(), truth[i]);
        }
    }
}