/*
 * Created on Thu Feb 22 2018
 *
 * Copyright (c) 2018 INRIA
 */

pub mod tag_extension;
pub mod tag_language;
pub mod tag_script;
pub mod tag_region;
pub mod tag_variant;
pub mod langtag;
pub mod langtag_builder;
pub mod language_tag;


//TagXXX setters do not ensure that input is well formed
//So these setters are not available outside of this crate

pub use self::tag_extension::TagExtension;
pub use self::tag_language::TagLanguage;
pub use self::tag_script::TagScript;
pub use self::tag_region::TagRegion;
pub use self::tag_variant::TagVariant;
pub use self::langtag::LangTag;
pub use self::langtag_builder::LangTagBuilder;
pub use self::language_tag::LanguageTag;