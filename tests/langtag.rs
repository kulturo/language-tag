extern crate language_tag;

use language_tag::tags::*;
use std::str::FromStr;

#[test]
fn test_tag_language() {
    let test_vectors = vec![
        "de",
        "fr-FR",
        "zh-cmn",
        "zh-cmn-Hans-CN",
        "zh-Hans-CN",
        "sl-rozaj-biske",
        "hy-Latn-IT-arevela",
        "es-419",
        "de-CH-x-phonebk",
        "zh-CN-a-myext-x-private",
        "en-a-myext-b-another",
        "sr-Cyrl",
        "zh-yue-HK",
        "de-CH-1901"
    ];
    let truth = vec![
        LangTagBuilder::new("de").build().unwrap(),
        LangTagBuilder::new("fr").region("FR").build().unwrap(),
        LangTagBuilder::new("zh-cmn").build().unwrap(),
        LangTagBuilder::new("zh").language_extension("cmn").script("Hans").region("cn").build().unwrap(),
        LangTagBuilder::new("zh").script("Hans").region("cn").build().unwrap(),
        LangTagBuilder::new("sl").variant("rozaj").variant("biske").build().unwrap(),
        LangTagBuilder::new("hy").script("Latn").region("IT").variant("arevela").build().unwrap(),
        LangTagBuilder::new("es").region("419").build().unwrap(),
        LangTagBuilder::new("de").region("CH").privateuse("x-phonebk").build().unwrap(),
        LangTagBuilder::new("zh").region("CN").extension("a-myext").privateuse("x-private").build().unwrap(),
        LangTagBuilder::new("en").extension("a-myext").extension("b-another").build().unwrap(),
        LangTagBuilder::new("sr").script("cyrl").build().unwrap(),
        LangTagBuilder::new("zh").region("HK").language_extension("yue").build().unwrap(),
        LangTagBuilder::new("de").region("CH").variant("1901").build().unwrap(),
    ];

    for (i, test) in test_vectors.iter().enumerate() {
        let parsed = LangTag::from_str(test);
        assert!(parsed.is_ok(), "Failed to parse {}: {}", test, parsed.err().unwrap());
        assert_eq!(parsed.unwrap(), truth[i], "{} failed to be parsed", test);
    }
}

#[test]
fn test_invalid() {
    let test_vectors = vec![
            "de-419-DE",
            "a-DE",
            "ar-a-aaa-b-bbb-a-ccc"
        ];

        for test in test_vectors.iter() {
            let parsed = LangTag::from_str(test);
            assert!(parsed.is_err(), "Succeed in parsing {} where it should fail", test);
        }
}